package br.com.calculadora.enumerated;

public enum OperacaoEnum {
    SOMA,
    SUBTRACAO,
    MULTIPLIACAO,
    DIVISAO;
}