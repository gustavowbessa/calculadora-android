package br.com.calculadora;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.calculadora.enumerated.OperacaoEnum;

public class MainActivity extends AppCompatActivity {

    private TextView visorResultado;
    private TextView visorHistorico;
    private boolean ultimaTeclaOperacao;
    private boolean modoResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        visorResultado = this.findViewById(R.id.textView);
        visorHistorico = this.findViewById(R.id.textView2);
        this.limparCampos();
    }

    public void onClickButton(View view) {

        String botaoPressionado = ((AppCompatButton) view).getText().toString();
        switch (botaoPressionado) {
            case ",":
                this.adicionarDecimal();
                break;
            case "C":
                this.limparCampos();
                break;
            case "/":
            case "+":
            case "x":
            case "-":
                this.adicionarOperacao(botaoPressionado);
                break;
            case "=":
                this.calcularResultado();
                modoResultado = true;
                break;
            default:
                this.adicionarNumero(botaoPressionado);
                break;
        }
    }

    private void adicionarDecimal() {
        if (modoResultado == true || ultimaTeclaOperacao == true) {
            visorResultado.setText("0");
            modoResultado = false;
        }
        if (!visorResultado.getText().toString().contains(".")) {
            visorResultado.append(".");
        }
        ultimaTeclaOperacao = false;
    }

    private void repetirOperacao(String equacao, String operacao, String regexOperacao) {
        String[] numeros = getNumerosEquacao(equacao, regexOperacao);
        if (numeros.length >= 2) {
            String primeiroNumero = visorResultado.getText().toString();
            numeros[0] = primeiroNumero;
            equacao = primeiroNumero.concat(operacao).concat(numeros[1]);
            visorHistorico.setText(equacao);
        }
    }

    private void somar(String equacao) {
        String[] numeros = getNumerosEquacao(equacao, "\\+");
        if (numeros.length >= 2) {
            Double resultado = new BigDecimal(numeros[0]).add(new BigDecimal(numeros[1])).doubleValue();
            if (resultado % resultado.intValue() == 0.0) {
                visorResultado.setText(String.valueOf(resultado.intValue()));
            } else {
                visorResultado.setText(resultado.toString());
            }
        }
    }

    private void subtrair(String equacao) {
        String[] numeros = getNumerosEquacao(equacao, "-");
        if (numeros.length >= 2) {
            Double resultado = new BigDecimal(numeros[0]).subtract(new BigDecimal(numeros[1])).doubleValue();
            if (resultado % resultado.intValue() == 0.0) {
                visorResultado.setText(String.valueOf(resultado.intValue()));
            } else {
                visorResultado.setText(resultado.toString());
            }
        }
    }

    private void multiplicar(String equacao) {
        String[] numeros = getNumerosEquacao(equacao, "x");
        if (numeros.length >= 2) {
            Double resultado = new BigDecimal(numeros[0]).multiply(new BigDecimal(numeros[1])).doubleValue();
            if (resultado % resultado.intValue() == 0.0) {
                visorResultado.setText(String.valueOf(resultado.intValue()));
            } else {
                visorResultado.setText(resultado.toString());
            }
        }
    }

    private void dividir(String equacao) {
        String[] numeros = getNumerosEquacao(equacao, "/");
        if (numeros.length >= 2) {
            Double resultado = new BigDecimal(numeros[0]).divide(new BigDecimal(numeros[1])).doubleValue();
            if (resultado % resultado.intValue() == 0.0) {
                visorResultado.setText(String.valueOf(resultado.intValue()));
            } else {
                visorResultado.setText(resultado.toString());
            }
        }
    }

    private void calcularResultado() {
        String equacao = visorHistorico.getText().toString();
        OperacaoEnum operacao = this.identificarOperacao(equacao);
        if (equacao.isEmpty() || operacao == null) {
            visorHistorico.setText("");
            visorHistorico.append(visorResultado.getText().toString());
        } else {
            if (equacao.contains("=")) {
                equacao = equacao.replaceAll("=", "");
                if (operacao.equals(OperacaoEnum.SOMA)) {
                    this.repetirOperacao(equacao, "+", "\\+");
                } else if (operacao.equals(OperacaoEnum.SUBTRACAO)) {
                    this.repetirOperacao(equacao, "-", "-");
                } else if (operacao.equals(OperacaoEnum.MULTIPLIACAO)) {
                    this.repetirOperacao(equacao, "x", "x");
                } else if (operacao.equals(OperacaoEnum.DIVISAO)) {
                    this.repetirOperacao(equacao, "/", "\\/");
                }
            } else {
                visorHistorico.append(visorResultado.getText().toString());
            }
            equacao = visorHistorico.getText().toString();
            if (operacao.equals(OperacaoEnum.SOMA)) {
                this.somar(equacao);
            } else if (operacao.equals(OperacaoEnum.SUBTRACAO)) {
                this.subtrair(equacao);
            } else if (operacao.equals(OperacaoEnum.MULTIPLIACAO)) {
                this.multiplicar(equacao);
            } else if (operacao.equals(OperacaoEnum.DIVISAO)) {
                this.dividir(equacao);
            }
        }

        visorHistorico.append("=");
    }

    private OperacaoEnum identificarOperacao(String equacao) {
        if (equacao.contains("+")) {
            return OperacaoEnum.SOMA;
        } else if (equacao.contains("-")) {
            return OperacaoEnum.SUBTRACAO;
        } else if (equacao.contains("x")) {
            return OperacaoEnum.MULTIPLIACAO;
        } else if (equacao.contains("/")) {
            return OperacaoEnum.DIVISAO;
        }
        return null;
    }

    private void limparCampos() {
        visorHistorico.setText("");
        visorResultado.setText("0");
        modoResultado = true;
        ultimaTeclaOperacao = false;
    }

    private void adicionarNumero(String numero) {
        String valor = visorResultado.getText().toString();
        String equacao = visorHistorico.getText().toString();
        if (valor.equals("0")) {
            visorResultado.setText(numero);
        } else {
            if (equacao.isEmpty()) {
                visorResultado.append(numero);
            } else {
                if (equacao.endsWith("=")) {
                    visorResultado.setText(numero);
                    if (this.identificarOperacao(equacao) != null) {
                        visorHistorico.setText("");
                    }
                } else {
                    if (modoResultado == true || ultimaTeclaOperacao == true) {
                        visorResultado.setText("");
                    }
                    visorResultado.append(numero);
                }
            }
        }
        modoResultado = false;
        ultimaTeclaOperacao = false;
    }

    private void adicionarOperacao(String operacao) {
        if (modoResultado == false && ultimaTeclaOperacao == false && !visorHistorico.getText().toString().isEmpty()) {
            this.calcularResultado();
        }
        visorHistorico.setText(visorResultado.getText().toString().concat(operacao));
        ultimaTeclaOperacao = true;
    }

    private String[] getNumerosEquacao(String equacao, String regexOperacao) {
        String[] numeros;
        if (equacao.startsWith("-")) {
            numeros = equacao.substring(1).split(regexOperacao);
            numeros[0] = "-".concat(numeros[0]);
        } else {
            numeros = equacao.split(regexOperacao);
        }
        return numeros;
    }
}